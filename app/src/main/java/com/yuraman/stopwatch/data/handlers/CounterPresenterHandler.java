package com.yuraman.stopwatch.data.handlers;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.yuraman.stopwatch.data.Counter;

public class CounterPresenterHandler extends Handler {

    public interface Callback {
        void inputCounter(String counter);
        void inputState(int state);
        void inputLap(String lap);
    }

    private Callback callback;

    public CounterPresenterHandler(Callback callback) {
        this.callback = callback;
    }

    @Override
    public void handleMessage(Message msg) {
        switch (msg.what) {
            case MessengerState.MSG_SEND_COUNTER:
                Bundle bundle = (Bundle) msg.obj;
                try {
                    // Workaround on duplicate key in bundle
                    callback.inputCounter(bundle.getString(Counter.COUNTER_VALUE));
                } catch (IllegalArgumentException cause){
                    cause.printStackTrace();
                }
                break;
            case MessengerState.MSG_SEND_STATE:
                callback.inputState(msg.arg1);
                break;
            case MessengerState.MSG_SEND_LAP:
                Bundle lapBundle = (Bundle) msg.obj;
                callback.inputLap(lapBundle.getString(Counter.COUNTER_LAP));
                break;
            default:
                break;
        }
    }
}
