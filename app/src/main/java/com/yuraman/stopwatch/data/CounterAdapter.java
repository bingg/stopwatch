package com.yuraman.stopwatch.data;

import android.annotation.SuppressLint;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.yuraman.stopwatch.R;
import java.util.List;

public class CounterAdapter extends RecyclerView.Adapter<CounterAdapter.CounterHolder> {
    private List<String> lapList;

    class CounterHolder extends RecyclerView.ViewHolder {
        TextView lap, counter;

        CounterHolder(@NonNull View itemView) {
            super(itemView);
            lap = itemView.findViewById(R.id.lap_counter_tv);
            counter = itemView.findViewById(R.id.counter_tv);
        }
    }

    public CounterAdapter(List<String> lapList) {
        this.lapList = lapList;
    }

    @NonNull
    @Override
    public CounterHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View lapView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.lap_item, viewGroup, false);
        return new CounterHolder(lapView);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(@NonNull CounterHolder counterHolder, int i) {
        String lap = lapList.get(i);
        counterHolder.lap.setText(String.format("%s %d", counterHolder.lap.getResources().getString(R.string.lap_tv), i + 1));
        counterHolder.counter.setText(lap);
    }


    @Override
    public int getItemCount() {
        return lapList.size();
    }
}
