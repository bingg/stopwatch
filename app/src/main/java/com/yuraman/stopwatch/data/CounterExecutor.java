package com.yuraman.stopwatch.data;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import com.yuraman.stopwatch.data.handlers.MessengerState;
import com.yuraman.stopwatch.util.Util;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicLong;

public class CounterExecutor {
    private static long FIXED_PERIOD = 10L;

    private Timer timer;
    private DBHandlerContract dbHandler;
    private AtomicLong atomicLong;
    private @Nullable Messenger presenterMessenger;
    private boolean isRegistered;
    private Bundle bundle;
    private ArrayList<String> laps;
    public Counter counter;
    public NotificationHelper notificationHelper;

    public CounterExecutor(Context context) {
        dbHandler = DBHandler.getInstance(context);
        counter = dbHandler.getCounter();
        laps = new ArrayList<>();
        notificationHelper = new NotificationHelper(context);
        notificationHelper.initNotification();
        if (!counter.getLap().isEmpty())laps.add(counter.getLap());
    }

    public void registerMessenger(Messenger presenterMessenger) {
        this.presenterMessenger = presenterMessenger;
        isRegistered = true;
        if (counter.getState() != MessengerState.MSG_STOP) {
            sendCounterValue(Counter.COUNTER_VALUE, Util.getConvertedCounter(counter.getValue()), MessengerState.MSG_SEND_COUNTER);
            sendState(counter.getState());
            sendCounterValue(Counter.COUNTER_LAP, counter.getLap(), MessengerState.MSG_SEND_LAP);
        }
    }

    public void unregisterMessenger(Messenger presenterMessenger) {
        this.presenterMessenger = presenterMessenger;
        isRegistered = false;
    }

    public void doStart() {
        timer = new Timer();
        counter.setState(MessengerState.MSG_START);
        atomicLong = new AtomicLong(counter.getValue());
        saveCounter();
        sendState(counter.getState());
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                long value = atomicLong.addAndGet(FIXED_PERIOD);
                counter.setValue(value);
                sendCounterValue(Counter.COUNTER_VALUE, Util.getConvertedCounter(counter.getValue()), MessengerState.MSG_SEND_COUNTER);
                notificationHelper.updateNotificationText(Util.getConvertedCounter(counter.getValue()));
            }
        }, 0, FIXED_PERIOD);
    }

    public void doPause(){
        counter.setState(MessengerState.MSG_PAUSE);
        sendState(counter.getState());
        saveCounter();
        freeResource();
    }

    public void doStop(){
        counter.setState(MessengerState.MSG_STOP);
        freeResource();
        dbHandler.deleteCounter();
        counter.setValue(0L);
        counter.setLap("");
        sendState(counter.getState());
        sendCounterValue(Counter.COUNTER_VALUE, Util.getConvertedCounter(counter.getValue()), MessengerState.MSG_SEND_COUNTER);;
    }

    public void doLap(){
        laps.add(String.valueOf(counter.getValue()));
        counter.setLap(TextUtils.join(",", laps));
        sendCounterValue(Counter.COUNTER_LAP, counter.getLap(), MessengerState.MSG_SEND_LAP);
    }

    public void saveCounter(){
        dbHandler.updateCounter(counter);
    }

    public void freeResource() {
        if (timer != null) {
            timer.cancel();
            timer = null;
            atomicLong = null;
        }
    }

    private void sendState(int counterState) {
        if (presenterMessenger != null) {
            try {
                presenterMessenger.send(Message.obtain(null, MessengerState.MSG_SEND_STATE, counterState, 0));
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    private void sendCounterValue(String counterKey, String counterValue, int msgState) {
        if (isRegistered && presenterMessenger != null) {
            bundle = new Bundle();
            bundle.putString(counterKey, counterValue);
            Message message = Message.obtain();
            message.obj = bundle;
            message.what = msgState;
            try {
                presenterMessenger.send(message);
            } catch (RemoteException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }
}
