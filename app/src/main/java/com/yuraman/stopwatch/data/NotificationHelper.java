package com.yuraman.stopwatch.data;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.yuraman.stopwatch.R;
import com.yuraman.stopwatch.counter.CounterActivity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class NotificationHelper {

    private NotificationManager notificationManager;
    private NotificationCompat.Builder notificationBuilder;
    private int notificationId;
    private String channelId = "counter_channel_id";
    private String channelName = "counter_channel_name";
    private Context context;

    public NotificationManager getNotificationManager() {
        return notificationManager;
    }

    public NotificationHelper(Context context) {
        notificationId = createID();
        this.context = context;
    }

    public void initNotification() {
        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(
                    channelId, channelName, NotificationManager.IMPORTANCE_LOW);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        notificationBuilder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.drawable.ic_lap)
                .setContentTitle(new StringBuilder(context.getString(R.string.app_name))
                        .append(" ")
                        .append(context.getString(R.string.running_title)))
                .setAutoCancel(false)
                .setColor(ContextCompat.getColor(context, R.color.colorPrimary));

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntent(new Intent(context, CounterActivity.class));
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.setContentIntent(resultPendingIntent);
    }

    private int createID() {
        Date now = new Date();
        return Integer.parseInt(new SimpleDateFormat("ddHHmmss", Locale.getDefault()).format(now));
    }

    public void updateNotificationText(String text){
        notificationBuilder.setContentText(text);
        notificationManager.notify(notificationId, notificationBuilder.build());
    }

    public int getNotificationId() {
        return notificationId;
    }

    public NotificationCompat.Builder getNotificationBuilder() {
        return notificationBuilder;
    }
}
