package com.yuraman.stopwatch.data.handlers;

public class MessengerState {
    public static final int MSG_REGISTER_CLIENT = 1;
    public static final int MSG_UNREGISTER_CLIENT = 2;
    public static final int MSG_START = 3;
    public static final int MSG_PAUSE = 4;
    public static final int MSG_STOP = 5;
    public static final int MSG_SEND_COUNTER = 6;
    public static final int MSG_SEND_STATE = 7;
    public static final int MSG_SEND_LAP = 8;
    public static final int MSG_ADD_LAP = 9;
}
