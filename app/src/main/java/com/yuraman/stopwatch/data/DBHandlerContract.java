package com.yuraman.stopwatch.data;

public interface DBHandlerContract {
    void addCounter(Counter counter);
    Counter getCounter();
    void updateCounter(Counter counter);
    void deleteCounter();

    enum Columns {
        ID, VALUE, STATE, LAP
    }
}
