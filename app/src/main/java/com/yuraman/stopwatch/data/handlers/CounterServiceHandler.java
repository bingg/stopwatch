package com.yuraman.stopwatch.data.handlers;

import android.os.Handler;
import android.os.Message;


public class CounterServiceHandler extends Handler {
    public interface Callback {
        void registerClient(Message msg);
        void unregisterClient();
        void onStart();
        void onPause();
        void onStop();
        void onAddLap();
    }

    private Callback callback;

    public CounterServiceHandler(Callback callback) {
        this.callback = callback;
    }

    @Override
    public void handleMessage(Message msg) {
        switch (msg.what) {
            case MessengerState.MSG_REGISTER_CLIENT:
                callback.registerClient(msg);
                break;
            case MessengerState.MSG_UNREGISTER_CLIENT:
                callback.unregisterClient();
                break;
            case MessengerState.MSG_START:
                callback.onStart();
                break;
            case MessengerState.MSG_PAUSE:
                callback.onPause();
                break;
            case MessengerState.MSG_STOP:
                callback.onStop();
                break;
            case MessengerState.MSG_ADD_LAP:
                callback.onAddLap();
                break;
            default:
                break;
        }
    }
}
