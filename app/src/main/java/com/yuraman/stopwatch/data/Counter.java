package com.yuraman.stopwatch.data;

import java.util.Objects;

public class Counter {
    public static final String COUNTER_VALUE = "counter_value";
    public static final String COUNTER_LAP = "counter_lap";

    private long value;
    private int state;
    private String lap;

    public Counter(long counter, int state, String lap) {
        this.value = counter;
        this.state = state;
        this.lap = lap;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public void setState(int state) {
        this.state = state;
    }

    public void setLap(String lap) {
        this.lap = lap;
    }

    public String getLap() {
        return lap;
    }

    public long getValue() {
        return value;
    }

    public int getState() {
        return state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Counter counter1 = (Counter) o;
        return value == counter1.value &&
                state == counter1.state;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, state);
    }
}
