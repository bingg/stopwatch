package com.yuraman.stopwatch.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.yuraman.stopwatch.data.handlers.MessengerState;


public class DBHandler extends SQLiteOpenHelper implements DBHandlerContract {

    private static DBHandler dbHelper;

    private static final String DB_NAME = "CounterDatabase";
    private static final String TABLE_NAME = "Counter";
    private static final int DB_VERSION = 1;


    public static synchronized DBHandler getInstance(Context context){
        if (dbHelper == null){
            dbHelper = new DBHandler(context);
        }
        return dbHelper;
    }

    private DBHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public synchronized void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
                    + Columns.ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + Columns.VALUE + " INTEGER,"
                    + Columns.STATE + " INTEGER," + Columns.LAP + " TEXT" + ")";
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public synchronized void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    @Override
    public synchronized void addCounter(Counter counter) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Columns.VALUE.name(), counter.getValue());
        values.put(Columns.STATE.name(), counter.getState());
        values.put(Columns.LAP.name(), counter.getLap());
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    @Override
    public synchronized Counter getCounter() {
        Counter counter = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, new String[] {
                Columns.VALUE.name(),
                Columns.STATE.name(),
                Columns.LAP.name() }, null, null, null, null, null);

        if (cursor != null){
            if (cursor.moveToFirst()){
                counter = new Counter(cursor.getLong(0), cursor.getInt(1),
                        cursor.getString(2));
            }
            else {
                counter = new Counter(0L, MessengerState.MSG_STOP, "");
                addCounter(counter);
            }
            cursor.close();
            db.close();
        }
        return counter;
    }

    @Override
    public synchronized void updateCounter(Counter counter) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Columns.VALUE.name(), counter.getValue());
        values.put(Columns.STATE.name(), counter.getState());
        values.put(Columns.LAP.name(), counter.getLap());
        db.update(TABLE_NAME, values, null, null);
        db.close();
    }

    @Override
    public synchronized void deleteCounter() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, null, null);
        db.close();
    }
}
