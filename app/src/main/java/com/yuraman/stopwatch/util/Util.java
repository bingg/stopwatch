package com.yuraman.stopwatch.util;

import android.annotation.SuppressLint;
import java.util.concurrent.TimeUnit;

public class Util {

    @SuppressLint("DefaultLocale")
    public static String getConvertedCounter(long counter){
        long small_millis = TimeUnit.MILLISECONDS.toMillis(counter) / 10 % 10;
        long millis = TimeUnit.MILLISECONDS.toMillis(counter) % 1000 / 100;
        long seconds = TimeUnit.MILLISECONDS.toSeconds(counter) % 60;
        long minutes = TimeUnit.MILLISECONDS.toMinutes(counter) % 60;
        long hours = TimeUnit.MILLISECONDS.toHours(counter);
        return String.format("%02d:%02d:%02d.%d%d", hours, minutes, seconds, millis, small_millis);
    }
}
