package com.yuraman.stopwatch.counter;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yuraman.stopwatch.R;
import com.yuraman.stopwatch.data.CounterAdapter;
import com.yuraman.stopwatch.service.CounterServiceManager;
import com.yuraman.stopwatch.service.CounterServiceManagerContract;

import java.util.ArrayList;
import java.util.List;

public class CounterActivity extends AppCompatActivity implements CounterActivityContract.MainActivityView {

    private CounterActivityPresenterImpl presenter;
    private CounterServiceManagerContract counterServiceManager;
    private TextView tvCounter, tvEmptyList;
    private MaterialButton btnStart, btnPause, btnStop;
    private FloatingActionButton btnAddLap;
    private List<String> laps;
    private RecyclerView recyclerView;
    private CounterAdapter counterAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        counterServiceManager = new CounterServiceManager(this);

        laps = new ArrayList<>();
        counterAdapter = new CounterAdapter(laps);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));
        recyclerView.setAdapter(counterAdapter);

        btnStart.setOnClickListener(v -> {
            presenter.handleStartCounter();
        });
        btnPause.setOnClickListener(v -> {
            presenter.handlePauseCounter();
        });
        btnStop.setOnClickListener(v -> {
            presenter.handleStopCounter();
        });
        btnAddLap.setOnClickListener(v -> {
            presenter.handleAddLap();
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter = new CounterActivityPresenterImpl(counterServiceManager);
        counterServiceManager.setCallback(presenter);
        counterServiceManager.setConnectionListener(presenter);
        presenter.subscribe(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.unsubscribe();
    }

    @Override
    public void setPresenter(CounterActivityPresenterImpl presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showEmptyLapList() {
        if (!laps.isEmpty()){
            laps.clear();
            counterAdapter.notifyDataSetChanged();
        }
        tvEmptyList.setVisibility(View.VISIBLE);
    }

    @Override
    public void addLap(List<String> counter) {
        if (tvEmptyList.getVisibility() == View.VISIBLE)tvEmptyList.setVisibility(View.INVISIBLE);
        if (!laps.toString().contentEquals(counter.toString())) {
            laps.clear();
            laps.addAll(counter);
            counterAdapter.notifyDataSetChanged();
        }
    }


    @Override
    public void enableBtnStart(){
        if (!btnStart.isEnabled()) {
            btnStart.setEnabled(true);
            btnStart.setStrokeColor(getResources().getColorStateList(R.color.colorPrimary));
            btnStart.setTextColor(getResources().getColorStateList(R.color.black));
        }
    }

    @Override
    public void disableBtnStart(){
        btnStart.setEnabled(false);
        btnStart.setStrokeColor(getResources().getColorStateList(R.color.grey));
        btnStart.setTextColor(getResources().getColorStateList(R.color.grey));
    }

    @Override
    public void updateCounter(String value) {
        tvCounter.setText(value);
    }

    @Override
    public void showAddLapBtn() {
        btnAddLap.show();
    }

    @Override
    public void hideAddLapBtn() {
        btnAddLap.hide();
    }

    private void init(){
        tvCounter = findViewById(R.id.counter_tv);
        btnStart = findViewById(R.id.start_btn);
        btnPause = findViewById(R.id.pause_btn);
        btnStop = findViewById(R.id.stop_btn);
        btnAddLap = findViewById(R.id.fab_btn);
        recyclerView = findViewById(R.id.counter_recycler_view);
        tvEmptyList = findViewById(R.id.empty_list);
    }
}
