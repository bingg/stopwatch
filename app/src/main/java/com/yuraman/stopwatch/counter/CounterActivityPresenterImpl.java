package com.yuraman.stopwatch.counter;

import com.yuraman.stopwatch.base.BaseView;
import com.yuraman.stopwatch.data.DBHandler;
import com.yuraman.stopwatch.data.handlers.CounterPresenterHandler;
import com.yuraman.stopwatch.data.handlers.MessengerState;
import com.yuraman.stopwatch.service.CounterServiceManagerContract;
import com.yuraman.stopwatch.util.Util;

import java.util.Arrays;
import java.util.List;


public class CounterActivityPresenterImpl implements CounterActivityContract.MainActivityPresenter, CounterPresenterHandler.Callback, CounterServiceManagerContract.ConnectionListener {

    private DBHandler dbHandler;
    private CounterActivityContract.MainActivityView view;
    private CounterServiceManagerContract serviceManagerContract;
    private int state;


    CounterActivityPresenterImpl(CounterServiceManagerContract serviceManager) {
        serviceManagerContract = serviceManager;
        dbHandler = DBHandler.getInstance(serviceManagerContract.getAppContext());
    }

    @Override
    public void handleStartCounter() {
        serviceManagerContract.startCounterServiceBind();
        serviceManagerContract.bindCounterServiceBind();
        state = MessengerState.MSG_START;
    }

    @Override
    public void handleStopCounter() {
        if (state == MessengerState.MSG_START) {
            serviceManagerContract.sendMessage(MessengerState.MSG_STOP);
            serviceManagerContract.unbindCounterServiceBind();
            serviceManagerContract.stopCounterServiceBind();
        }
        if (state == MessengerState.MSG_PAUSE || dbHandler.getCounter().getState() == MessengerState.MSG_PAUSE){
            dbHandler.deleteCounter();
        }
        view.showEmptyLapList();
        view.updateCounter(Util.getConvertedCounter(0L));
    }

    @Override
    public void handlePauseCounter() {
        if (state == MessengerState.MSG_START) {
            serviceManagerContract.sendMessage(MessengerState.MSG_PAUSE);
            serviceManagerContract.unbindCounterServiceBind();
            serviceManagerContract.stopCounterServiceBind();
        }
    }

    @Override
    public void handleAddLap() {
        if (state == MessengerState.MSG_START) {
            serviceManagerContract.sendMessage(MessengerState.MSG_ADD_LAP);
        }
    }

    @Override
    public void subscribe(BaseView view) {
        this.view = (CounterActivityContract.MainActivityView) view;
        this.view.setPresenter(this);
        switch (dbHandler.getCounter().getState()){
            case MessengerState.MSG_START:
                serviceManagerContract.bindCounterServiceBind();
                this.view.showEmptyLapList();
                break;
            case MessengerState.MSG_PAUSE:
                this.view.updateCounter(Util.getConvertedCounter(dbHandler.getCounter().getValue()));
                inputLap(dbHandler.getCounter().getLap());
                this.view.hideAddLapBtn();
                break;
            case MessengerState.MSG_STOP:
                this.view.showEmptyLapList();
                this.view.hideAddLapBtn();
            default:
                break;
        }
    }

    @Override
    public void unsubscribe() {
        if (state == MessengerState.MSG_START){
            serviceManagerContract.unbindCounterServiceBind();
        }
        if (view != null){
            view.setPresenter(null);
            view = null;
        }
    }

    @Override
    public void inputCounter(String counter) {
        if (counter != null && view != null){
           view.updateCounter(counter);
        }
    }

    @Override
    public void inputState(int state) {
        if (this.state != state)this.state = state;
        switch (state){
            case MessengerState.MSG_START:
                view.disableBtnStart();
                view.showAddLapBtn();
                break;
            case MessengerState.MSG_PAUSE:
                view.enableBtnStart();
                view.hideAddLapBtn();
                break;
            case MessengerState.MSG_STOP:
                view.enableBtnStart();
                view.hideAddLapBtn();
                break;
            default:
                break;
        }
    }

    @Override
    public void inputLap(String lap) {
        if (view != null && lap != null) {
            if (lap.isEmpty()){
                view.showEmptyLapList();
                return;
            }
            List<String> tmpLap = Arrays.asList(lap.split(","));
            for (int i = 0; i < tmpLap.size(); i++){
                tmpLap.set(i, Util.getConvertedCounter(Long.parseLong(tmpLap.get(i))));
            }
            view.addLap(tmpLap);
        }
    }

    @Override
    public void onSuccessfullyConnected() {
        if (state == MessengerState.MSG_START) {
            serviceManagerContract.sendMessage(MessengerState.MSG_START);
        }
    }
}
