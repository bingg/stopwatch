package com.yuraman.stopwatch.counter;

import com.yuraman.stopwatch.base.BasePresenter;
import com.yuraman.stopwatch.base.BaseView;

import java.util.List;


public class CounterActivityContract {
    public interface MainActivityView extends BaseView<CounterActivityPresenterImpl> {
        void showEmptyLapList();
        void addLap(List<String> counter);
        void enableBtnStart();
        void disableBtnStart();
        void updateCounter(String value);
        void showAddLapBtn();
        void hideAddLapBtn();
    }

    public interface MainActivityPresenter extends BasePresenter {
        void handleStartCounter();
        void handleStopCounter();
        void handlePauseCounter();
        void handleAddLap();
    }
}
