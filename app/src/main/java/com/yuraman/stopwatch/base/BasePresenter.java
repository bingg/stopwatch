package com.yuraman.stopwatch.base;

public interface BasePresenter {
    void subscribe(BaseView view);
    void unsubscribe();
}
