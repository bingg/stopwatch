package com.yuraman.stopwatch.base;

public interface BaseView<T> {
    void setPresenter(T presenter);
}
