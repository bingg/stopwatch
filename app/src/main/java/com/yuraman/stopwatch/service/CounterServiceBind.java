package com.yuraman.stopwatch.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import androidx.annotation.Nullable;

import com.yuraman.stopwatch.data.CounterExecutor;
import com.yuraman.stopwatch.data.handlers.CounterServiceHandler;
import com.yuraman.stopwatch.data.handlers.MessengerState;


public class CounterServiceBind extends Service implements CounterServiceHandler.Callback {

    private @Nullable Messenger presenterMesenger;
    private CounterExecutor counterExecutor;

    final Messenger counterServiceMessenger = new Messenger(new CounterServiceHandler(this));

    @Override
    public IBinder onBind(Intent intent) {
        return counterServiceMessenger.getBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return false;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        counterExecutor = new CounterExecutor(this);
        startForeground(counterExecutor.notificationHelper.getNotificationId(), counterExecutor.notificationHelper.getNotificationBuilder().build());
        int state = counterExecutor.counter.getState();
        if (state == MessengerState.MSG_START) {
            try {
                counterServiceMessenger.send(Message.obtain(null, state));
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        handleStopService();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        handleStopService();
    }


    @Override
    public void registerClient(Message msg) {
        presenterMesenger = msg.replyTo;
        counterExecutor.registerMessenger(presenterMesenger);
    }

    @Override
    public void unregisterClient() {
        presenterMesenger = null;
        counterExecutor.unregisterMessenger(presenterMesenger);
    }

    @Override
    public void onStart() {
        counterExecutor.doStart();
    }

    @Override
    public void onPause() {
        counterExecutor.doPause();
    }

    @Override
    public void onStop() {
        counterExecutor.doStop();
    }

    @Override
    public void onAddLap() {
        counterExecutor.doLap();
    }

    private void handleStopService(){
        counterExecutor.saveCounter();
        counterExecutor.notificationHelper.getNotificationManager().cancelAll();
    }
}
