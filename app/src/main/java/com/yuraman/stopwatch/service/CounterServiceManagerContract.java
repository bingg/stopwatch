package com.yuraman.stopwatch.service;

import android.content.Context;

import com.yuraman.stopwatch.data.handlers.CounterPresenterHandler;

public interface CounterServiceManagerContract {
    interface ConnectionListener{
        void onSuccessfullyConnected();
    }
    void startCounterServiceBind();
    void stopCounterServiceBind();
    void bindCounterServiceBind();
    void unbindCounterServiceBind();
    void setCallback(CounterPresenterHandler.Callback callback);
    void setConnectionListener(ConnectionListener connectionListener);
    void sendMessage(int msg);
    Context getAppContext();
}
