package com.yuraman.stopwatch.service;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import androidx.annotation.Nullable;

import com.yuraman.stopwatch.data.handlers.CounterPresenterHandler;
import com.yuraman.stopwatch.data.handlers.MessengerState;

public class CounterServiceManager implements ServiceConnection, CounterServiceManagerContract {

    private Context appContext;
    private @Nullable Messenger counterServiceMessenger;
    private Messenger presenterMessenger;
    private boolean isBound;
    private CounterServiceManagerContract.ConnectionListener connectionListener;

    public CounterServiceManager(Context context) {
        appContext = context;
    }

    public void startCounterServiceBind(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            appContext.startForegroundService(new Intent(appContext, CounterServiceBind.class));
        }
        else {
            appContext.startService(new Intent(appContext, CounterServiceBind.class));
        }
    }

    @Override
    public void stopCounterServiceBind() {
        appContext.stopService(new Intent(appContext, CounterServiceBind.class));
    }

    @Override
    public void bindCounterServiceBind(){
        appContext.bindService(new Intent(appContext, CounterServiceBind.class), this, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void unbindCounterServiceBind(){
        if (isBound){
            sendMessage(MessengerState.MSG_UNREGISTER_CLIENT);
            appContext.unbindService(this);
            isBound = false;
        }
    }

    @Override
    public Context getAppContext() {
        return appContext;
    }

    @Override
    public void setCallback(CounterPresenterHandler.Callback callback) {
        presenterMessenger = new Messenger(new CounterPresenterHandler(callback));
    }

    @Override
    public void setConnectionListener(ConnectionListener connectionListener) {
        this.connectionListener = connectionListener;
    }

    @Override
    public void sendMessage(int msg) {
        if (counterServiceMessenger != null) {
            try {
                counterServiceMessenger.send(Message.obtain(null, msg));
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        counterServiceMessenger = new Messenger(service);
        Message message = Message.obtain();
        message.replyTo = presenterMessenger;
        message.what = MessengerState.MSG_REGISTER_CLIENT;
        try {
            counterServiceMessenger.send(message);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        isBound = true;
        connectionListener.onSuccessfullyConnected();
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        counterServiceMessenger = null;
        isBound = false;
    }
}
